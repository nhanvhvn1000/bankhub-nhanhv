"use client";
import { useEffect, useState } from 'react';
import axios from 'axios';
import Navigation from '@/components/Navigation';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import BalanceTable from '@/components/BalanceTable';
import RootLayout from './layout';
import TransactionTable from '@/components/TransactionTable';


export default function Home() {
  const [iframeLoading, setIframeLoading] = useState(false)
  const [bankhubLink, setBankhubLink] = useState()
  const [accessToken, setAccessToken] = useState(null)
  const [data, setData] = useState()

  const accountInfo = data !== undefined ? data.accounts[0] : null
  const transactions = data !== undefined ? data.transactions.map(({ reference, transactionDateTime, amount, description, runningBalance }) => ({ reference, transactionDateTime, amount, description, runningBalance })) : []

  const handleLinkBank = () => {
    const config = {
      method: 'get',
      url: 'http://localhost:5000/grant/token',
    }

    axios(config).then((response) => {
      setBankhubLink(response.data)
      setIframeLoading(true)

    })
  }

  const handleGetTrans = () => {
    const accessToken = localStorage.getItem('accessToken')
    const config = {
      method: 'post',
      url: 'http://localhost:5000/transactions',
      data: {
        accessToken: accessToken
      }
    }

    axios(config).then((response) => {
      // console.log(response.data)
      setData(response.data)
    })
  }

  const handleMessage = (event) => {
    if (event.origin !== "https://dev.link.bankhub.dev") return;
    // console.log(event.data)
    const jsonResponse = JSON.parse(event.data)
    const publicToken = jsonResponse.data.publicToken
    // console.log(jsonResponse.data.publicToken)
    // console.log(publicToken)

    const config = {
      method: 'post',
      url: 'http://localhost:5000/grant/exchange',
      headers: {
        "Content-Type": "application/json"
      },
      data: {
        publicToken: publicToken,
      }
    }

    axios(config).then((response) => {
      // console.log(response)
      setAccessToken(response.data)
      localStorage.setItem("accessToken", response.data)
      setIframeLoading(false)

    })

  }

  useEffect(() => {
    const accessToken = localStorage.getItem('accessToken')
    if (accessToken) setAccessToken(accessToken)
    window.addEventListener('message', handleMessage)

    return () => {
      window.removeEventListener('message', handleMessage);
    };
  }, [])

  return (
    <RootLayout>
      <main className="flex items-center flex-col bg-[#DDE6ED] w-screen min-h-screen pb-20">
        <Navigation />
        {iframeLoading && <iframe src={bankhubLink} title="Bankhub Frame"
          className='h-full w-full fixed block z-[1000] overflow-hidden top-0 bottom-0 left-0 right-0'></iframe>}

        <div className="flex flex-col w-9/12 mt-20">
          <div className='my-5'>
            <h1 className='font-bold'>Bước 1: Kết nối tài khoản</h1>
            <button className="w-40 text-base text-white bg-[#27374D] rounded-md p-2 hover:bg-[#9DB2BF] duration-200 hover:cursor-pointer my-2" onClick={handleLinkBank}>Liên kết ngân hàng</button>
            {accessToken && <h3>Đã liên kết</h3> || <h3>Chưa liên kết</h3>}
          </div>
          <div className='my-5'>
            <h1 className='font-bold'>Bước 2: Truy vấn thông tin giao dịch</h1>
            <button className="w-40 text-base text-white bg-[#27374D] rounded-md p-2 hover:bg-[#9DB2BF] duration-200 hover:cursor-pointer my-2 disabled:cursor-not-allowed disabled:opacity-40" onClick={handleGetTrans} disabled={accessToken == null}>Truy vấn giao dịch</button>
          </div>

        </div>

        <div className='w-10/12 items-center flex flex-col'>
          {data !== undefined && <div className='my-3 w-6/12'><BalanceTable data={accountInfo} /></div>}
          {data !== undefined && <TransactionTable transactions={transactions} />}
        </div>

      </main>
    </RootLayout>
  )
}

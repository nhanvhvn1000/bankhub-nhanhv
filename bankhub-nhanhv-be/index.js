import express from 'express';
import cors from 'cors'
import axios from 'axios';

const app = express()
const port = 5000

app.use(cors())
app.use(express.json());

app.get('/grant/token', (req, res) => {
    let data = JSON.stringify({
        "scopes": "transaction",
        "redirectUri": "http://localhost:3000",
        "language": "vi"
    });

    let config = {
        method: 'post',
        maxBodyLength: Infinity,
        url: 'https://sandbox.bankhub.dev/grant/token',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'x-client-id': '356b7f1a-0a81-11ee-b8b7-42010a40001b',
            'x-secret-key': '356b7f34-0a81-11ee-b8b7-42010a40001b'
        },
        data: data
    };

    axios(config)
        .then((response) => {
            const redirectUri = "http://localhost:3000"
            const grantToken = response.data.grantToken
            const bankhubLink = `https://dev.link.bankhub.dev?redirectUri=${redirectUri}&grantToken=${grantToken}&iframe=true`
            res.send(bankhubLink)
        })
        .catch((error) => {
            // console.log(error);
        });

})

app.post('/grant/exchange', (req, res) => {
    let data = req.body

    // console.log(data)

    let config = {
        method: 'post',
        maxBodyLength: Infinity,
        url: 'https://sandbox.bankhub.dev/grant/exchange',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'x-client-id': '356b7f1a-0a81-11ee-b8b7-42010a40001b',
            'x-secret-key': '356b7f34-0a81-11ee-b8b7-42010a40001b'
        },
        data: data
    };

    axios(config)
        .then((response) => {
            // console.log(response.data)
            res.send(response.data.accessToken)
        })
        .catch((error) => {
            // console.log(error);
        });
})

app.post('/transactions', (req, res) => {
    const accessToken = req.body.accessToken
    // console.log('AccessToken', accessToken)
    let config = {
        method: 'get',
        maxBodyLength: Infinity,
        url: 'https://sandbox.bankhub.dev/transactions',
        headers: {
            'Accept': 'application/json',
            'x-client-id': '356b7f1a-0a81-11ee-b8b7-42010a40001b',
            'x-secret-key': '356b7f34-0a81-11ee-b8b7-42010a40001b',
            'Authorization': accessToken
        }
    };

    console.log(config)

    axios(config)
        .then((response) => {
            res.send(JSON.stringify(response.data))
        })
        .catch((error) => {
            console.log(error);
        });
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})